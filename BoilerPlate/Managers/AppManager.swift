//
//  AppManager.swift
//  BoilerPlate
//
//  Created by Vijay Raj on 2017-08-14.
//  Copyright © 2017 Vijay Raj. All rights reserved.
//

import Foundation

private let _singleton = AppManager()

class AppManager {
    private let AUTH_TOKEN = "AUTH_TOKEN_B"
    private let REFRESH_TOKEN = "REF_TOKEN_B"
    
    // JWT
    private var JWT_MGR:JWTManager?
    
    
    // An array of all the contacts from the backend
    var contacts: [Contact] = [Contact]()
    
//    var userCache: [String: User] = [String: User]()
    
    // Cache of users
    
    class var single: AppManager {
        return _singleton
    }
    
    func getAuthToken() -> String? {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: AUTH_TOKEN)
    }
    
    func getRefreshToken() -> String? {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: REFRESH_TOKEN)
    }
    
    func setAuthToken(with token: String, refresh: String) -> Bool {
        let jwtManager = JWTManager(with: token)
        let isActive = jwtManager.isActive()
        
        self.setRefreshToken(token: refresh)
        self.setAuthToken(token: token)
        
        return isActive
    }
    
    func clearAuthTokens() {
        self.setRefreshToken(token: "")
        self.setAuthToken(token: "")
    }
    
    func setRefreshToken(token: String) {
        let defaults = UserDefaults.standard
        defaults.setValue(token, forKey: REFRESH_TOKEN)
    }
    
    func setAuthToken(token: String) {
        let defaults = UserDefaults.standard
        defaults.setValue(token, forKey: AUTH_TOKEN)
    }
    
}
