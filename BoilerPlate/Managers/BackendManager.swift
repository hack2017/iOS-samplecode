//
//  BackendManager.swift
//  BoilerPlate
//
//  Created by Vijay Raj on 2017-08-14.
//  Copyright © 2017 Vijay Raj. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

// This is a singleton manager to perform all interactions with backend

private let _singleton = BackendManager()

class BackendManager {
    
    fileprivate let AUTH_URL = "https://hack2017.mbenablers.com"
    fileprivate let AUTH_TOKEN = "/tokens"
    fileprivate let CONTACTS = "/contacts"
    
    class var single: BackendManager {
        return _singleton
    }
    
    // Perform Login
    func performLogin(username: String, password: String, callback: @escaping (Bool) -> Void) {
        let parameters = ["username": username, "password": password]
        
        Alamofire.request(AUTH_URL + AUTH_TOKEN, method: .post, parameters: parameters, encoding: JSONEncoding.default)
        .responseJSON { (response) in
            
                if let jsonvalue = response.result.value {
                    print("JSON \(jsonvalue)")
                    
                    // Parse accessToken and put it
                    let json = JSON(jsonvalue)
                    
                    if json["error"] != JSON.null {
                        // error
                        callback(true)
                        return
                    }
                    
                    let access_token = json["tokens"]["accessToken"].string
                    print("Access Token: \(access_token!)")
                    
                    let refresh_token = json["tokens"]["refreshToken"].string
                    print("Refresh Token: \(refresh_token!)")
                    
                    let isActive = AppManager.single.setAuthToken(with: access_token!, refresh: refresh_token!)
                    
                    /*
                     BackendManager.single.refresh(callback: { (error) in
                     
                     
                     if error == true {
                     print("Error \(error)")
                     return
                     }
                     print("Execute callback function")
                     })
                     */
                    
                    callback(!isActive)
                    return
                }
                
                callback(true)
        }
    }
    
    func refresh(callback: @escaping (Bool) -> Void){
        let headers = ["x-access-token": AppManager.single.getAuthToken()!]
        
        Alamofire.request(URL(string: AUTH_URL + AUTH_TOKEN)!, method: .put, encoding: URLEncoding.default, headers: headers)
            .responseJSON{ response in
                print(response.request!)
                print(response.response!)
                print(response.data!)
                print(response.result)
                
                if let jsonvalue = response.result.value {
                    
                    // Parse accessToken and put it
                    let json = JSON(jsonvalue)
                    print("Refresh Token JSON: \(json)")
                    
                    
                    let access_token = json["tokens"]["accessToken"].string
                    let refresh_token = json["tokens"]["refreshToken"].string
                    print("Access Token: \(access_token!)")
                    let active = AppManager.single.setAuthToken(with: access_token!, refresh: refresh_token!)
                    callback(!active)
                    
                    return
                }
                
        }
        
    }
    
    func makeAuthenticatedCall(with callback: @escaping (Bool, String) -> Void) {
        let headers = ["Authorization": "Bearer " + AppManager.single.getAuthToken()!]
        let jwt = JWTManager(with: AppManager.single.getAuthToken()!)
        let urlstr = AUTH_URL + "/users/" + String(jwt.getId()) + CONTACTS
        
        Alamofire.request(urlstr, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            
            if response.result.value == nil {
                callback(false, "Error")
                return
                
            }
            
            let json = JSON(response.result.value!)
            if json["error"] != JSON.null {
                // error
                callback(false, "Error")
                return
            }
            
            callback(true, json.debugDescription)
        }
        
    }
    
    func populateContacts(_ callback: @escaping (Bool) -> Void) {
        
        let headers = ["Authorization": "Bearer " + AppManager.single.getAuthToken()!]
        
        
        Alamofire.request(AUTH_URL, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            
                if response.result.value == nil {
                    callback(true)
                    return
                
                }
                
                let json = JSON(response.result.value!)
                if json["error"] != JSON.null {
                    // error
                    callback(true)
                    return
                }
                
                
                // Populate
                let contacts = json["contacts"].array
            
            if contacts == nil {
                callback(false)
                return
            }
                
                for contact in contacts! {
                    let newContact = Contact()
                    newContact.address = contact["address"] == JSON.null ? "" : contact["address"].string!
                    newContact.city = contact["city"] == JSON.null ? "" : contact["city"].string!
                    newContact.name = contact["name"] == JSON.null ? "" : contact["name"].string!
                    
                    print("loc: \(newContact.name!) | \(newContact.address!) | \(newContact.city!)")
                    AppManager.single.contacts.append(newContact)
                }
                
                // No error
                callback(false)
        
            // Populate array of locations
            
        }
    }
    
}
