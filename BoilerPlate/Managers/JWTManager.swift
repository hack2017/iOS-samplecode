//
//  JWTManager.swift
//  BoilerPlate
//
//  Created by Vijay Raj on 2017-08-14.
//  Copyright © 2017 Vijay Raj. All rights reserved.
//

import Foundation
import JWTDecode

class JWTManager {
    internal var jwt:JWT?
    internal var jwtBody:[String: Any]?
    
    init(with token:String) {
        self.parse(for: token)
    }
    
    func parse(for token:String) -> Void {
        do{
            self.jwt = try decode(jwt: token)
            self.jwtBody = self.jwt?.body
            print(self.jwtBody.debugDescription)
        }catch _ {
            
        }
    }
    
    func isActive() -> Bool {
        let active = self.jwtBody?["isActive"] as! Bool
        return active
    }
    
    func isAdmin() -> Bool {
        let admin = self.jwtBody?["isAdmin"] as! Bool
        return admin
    }
    
    func getId() -> Int{
        let id = self.jwtBody?["userId"]
        return id as! Int
    }

    
    func getUsername() -> String{
        let username = self.jwtBody?["username"]
        return username as! String
    }
    
    func getExpiry() -> Int {
        let exp = self.jwtBody?["exp"]
        return exp as! Int
    }
}
