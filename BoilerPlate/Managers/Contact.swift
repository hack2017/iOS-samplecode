//
//  Contact.swift
//  BoilerPlate
//
//  Created by Vijay Raj on 2017-08-14.
//  Copyright © 2017 Vijay Raj. All rights reserved.
//

import Foundation

class Contact {
    var address:String?
    var city: String?
    var name: String?
}
