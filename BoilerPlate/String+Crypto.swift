//
//  String+Crypto.swift
//  BoilerPlate
//
//  Created by Vijay Raj on 2017-08-14.
//  Copyright © 2017 Vijay Raj. All rights reserved.
//

import Foundation
import CryptoSwift

internal extension String {
    
    internal func aesEncrypt(_ key: String, iv: String) throws -> String {
        let data = self.data(using: String.Encoding.utf8)
        let enc = try AES(key: key, iv: iv, blockMode:.CBC).encrypt(data!)
        let encData = Data(bytes: UnsafePointer<UInt8>(enc), count: Int(enc.count))
        let base64String: String = encData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0));
        let result = String(base64String)
        return result!
    }
    
    internal func aesDecrypt(_ key: String, iv: String) throws -> String? {
        guard let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) else {
            print("base64 decode failure")
            return nil
        }
        
        let dec = try AES(key: key, iv: iv, blockMode: .CBC).decrypt(data)
        let decData = Data(bytes: UnsafePointer<UInt8>(dec), count: Int(dec.count))
        if let result = NSString(data: decData, encoding: String.Encoding.utf8.rawValue) {
            return result as String
        }
        else {
            return nil
        }
    }
}
