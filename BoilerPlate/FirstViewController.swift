//
//  FirstViewController.swift
//  BoilerPlate
//
//  Created by Vijay Raj on 2017-08-14.
//  Copyright © 2017 Vijay Raj. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var userInfoView: UITextView!
    @IBOutlet weak var apiCallOutput: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    /**
     1. Check if user is logged in, show login page otherwise
     2. Show spinner, populate sales leads, hide spinner
     3. Associate with table view
     4. Have fun !!
     
     - parameter animated:
     */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        // No auth token, show login.
        if AppManager.single.getAuthToken() == nil || AppManager.single.getAuthToken() == "" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loginController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
            self.present(loginController, animated: true, completion: {
                print("Invoked loginview controller")
            })
            return
        }
        
        // JWT info
        let user = JWTManager(with: AppManager.single.getAuthToken()!).getUsername()
        
        self.userInfoView.text = "Logged in as: \(user) \ntoken: \(String(describing: AppManager.single.getAuthToken())))"
        
    }

    @IBAction func doLogout(_ sender: UIButton) {
        AppManager.single.clearAuthTokens()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
        self.present(loginController, animated: true, completion: {
            print("Invoked loginview controller")
        })
    }
    
    @IBAction func makeAuthenticatedCall(_ sender: UIButton) {
        BackendManager.single.makeAuthenticatedCall { (error, value) in
            self.apiCallOutput.text = value
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

