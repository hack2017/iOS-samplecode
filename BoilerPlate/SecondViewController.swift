//
//  SecondViewController.swift
//  BoilerPlate
//
//  Created by Vijay Raj on 2017-08-14.
//  Copyright © 2017 Vijay Raj. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var text_to_encrypt: UITextField!
    @IBOutlet weak var iv_label: UILabel!
    @IBOutlet weak var encrypted_str_tview: UITextView!
    @IBOutlet weak var decrypted_str_tview: UITextView!
    @IBOutlet weak var encryption_key_label: UILabel!
    
    @IBOutlet weak var decryptButton: UIButton!
    internal var cryptoKey:String?
    internal var ivString: String?
    internal var encryptedStr: String?
    internal var decryptedStr: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.decryptButton.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Method uses iOS Randomized Services to generate a cryptographically secure random number
    func generateFileEncryptionKey() -> String {
        
        let bytesCount = 16 // 16 bytes --> 32byte string --> 256bit AES encryption
        var randomNum = ""
        var randomBytes = [UInt8](repeating: 0, count: bytesCount)
        let result = SecRandomCopyBytes(kSecRandomDefault, bytesCount, &randomBytes)
        
        if result == 0 {
            
            // Map to hex values to return a string
            randomNum = randomBytes.map({String(format: "%02hhx", $0)}).joined(separator: "")
        }
        return randomNum
    }
    
    func getRandomIV(_ encryptionId: String? = nil) -> (random: String, timeStamp: Int) {
        let timeStamp = Int(Date().timeIntervalSince1970)
        let random = "\(timeStamp)\(timeStamp)".md5()
        return (random: random.substring(to: random.characters.index(random.startIndex, offsetBy: 16)), timeStamp: timeStamp)
    }


    @IBAction func encrypt(_ sender: UIButton) {
        
        if (self.text_to_encrypt.text?.isEmpty)!
        {
            return
        }
        
        self.cryptoKey = generateFileEncryptionKey()
        self.encryption_key_label.text = self.cryptoKey
        // IV
        let random = getRandomIV()
        self.ivString = random.random
        self.iv_label.text = "IV: \(String(describing: self.ivString!))"
        // Encrypt
        do {
            self.encryptedStr = try self.text_to_encrypt.text?.aesEncrypt(self.cryptoKey!, iv: self.ivString!)
            self.encrypted_str_tview.text = "Encrypted Str: \(String(describing: encryptedStr!))"
            self.decryptButton.isEnabled = true
        } catch {
            print("error")
        }
        
    }

    @IBAction func decrypt(_ sender: UIButton) {
        do{
            self.decryptedStr = try self.encryptedStr?.aesDecrypt(self.cryptoKey!, iv: self.ivString!)
            self.decrypted_str_tview.text = "Decrypted Str: \(String(describing: decryptedStr!))"
        } catch {
            print("error")
        }
    }
    
}

