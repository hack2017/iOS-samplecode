//
//  LoginViewController.swift
//  BoilerPlate
//
//  Created by Vijay Raj on 2017-08-14.
//  Copyright © 2017 Vijay Raj. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var loadingSpinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        password.isSecureTextEntry = true
        errorLabel.isHidden = true
        loadingSpinner.isHidden = true
        loadingSpinner.startAnimating()
    }
    
    /**
     Perform login
     
     - parameter sender: anyobject
     */
    @IBAction func doLogin(_ sender: AnyObject) {
        self.loadingSpinner.isHidden = false
        BackendManager.single.performLogin(username: self.username.text!, password: self.password.text!) { (error) in
            
            self.loadingSpinner.isHidden = true
            if error == true {
                self.errorLabel.text = "Error... Please try again."
                self.errorLabel.isHidden = false
                
                return
            }
            
            
            self.dismiss(animated: true) {
                print("Closing login view controller.")
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
