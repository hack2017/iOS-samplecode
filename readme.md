# iOS Basic sample app

This app is a starting point for an user wanting to use the hackathon backend on iOS. A few components have been implemented for a developer to pick up and extend, or use as reference.

## Pods

This project uses cocoapods and some common pods. The developer does not need to intall cocoapods to use the project or extend it. Developer will need to open *BoilerPlate.xcworkspace* file in XCode instead of the project file to have the pods loaded and to build. These are the pods used:

- Alamofire - Networking helper framework
- SwiftyJSON - JSON parser framework
- JWTDecode - JWT decoder framework
- CryptoSwift - Basic crypto framework using best practices

## Components in the project

- Authentication
- JWT reading
- API call with token
- Local crypto functions

## Structure

- Managers/AppManager.swift - local app state store logic
- Managers/BackendManager.swift - all API / Auth / backend interactions
- Managers/JWTManager.swift - JWT parsing from token
- String+Crypto.swift - AES Encryption and Decryption - extends String to be used as string.encrypt
- Views/FirstViewController.swift - UI and call start for Authenticated API call and JWT read
- Views/SecondViewController.swift - UI and calls for local crypto

